import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import LocomotiveScroll from "locomotive-scroll";
import { useEffect } from "react";

const addVirtualScrollOnWebsite = () => {
  console.log("addVirtualScrollOnWebsite");
  const locoScroll = new LocomotiveScroll({
    el:
      (document.querySelector(`[data-scroll-container]`) as HTMLElement) ??
      null,

    smooth: true,
    lerp: 0.01,

    tablet: {
      smooth: true,
      breakpoint: 768,
    },
    smartphone: {
      smooth: true,
    },
  });

  console.log({ locoScroll });

  locoScroll.on("scroll", ScrollTrigger.update);

  //@ts-ignore
  ScrollTrigger.scrollerProxy(locoScroll.el, {
    scrollTop(value: any) {
      return arguments.length
        ? locoScroll.scrollTo(value, { duration: 0 })
        : //@ts-ignore
          locoScroll.scroll.instance.scroll.y;
    },
    getBoundingClientRect() {
      return {
        top: 0,
        left: 0,
        width: window.innerWidth,
        height: window.innerHeight,
      };
    },
  });

  return locoScroll;
};

const BVirtualScrollOnlyInClient = () => {
  useEffect(() => {
    const locoScroll = addVirtualScrollOnWebsite();

    const syncLocoWithGSAP = () => {
      locoScroll && locoScroll.update();
    };

    ScrollTrigger.defaults({ scroller: locoScroll.el });

    ScrollTrigger.addEventListener("refresh", syncLocoWithGSAP);

    ScrollTrigger.refresh();

    return () => {
      ScrollTrigger.removeEventListener("refresh", syncLocoWithGSAP);
      locoScroll.destroy();
    };
  });

  return null;
};

export default BVirtualScrollOnlyInClient;
