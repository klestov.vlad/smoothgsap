import dynamic from "next/dynamic";
import { ReactNode } from "react";

const BVirtualScrollOnlyInClient = dynamic(
  () => import("./b-virtual-scroll-only-in-client"),
  { ssr: false }
);

interface BVirtualScrollProps {
  children: ReactNode;
}

export const BVirtualScroll = ({ children }: BVirtualScrollProps) => {
  const p = "b-virtual-scroll";

  return (
    <>
      <div className={`${p} | js-${p}`} data-scroll-container>
        {children}
      </div>
      <BVirtualScrollOnlyInClient />
    </>
  );
};
