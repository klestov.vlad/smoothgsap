interface SectionProps {
  color: string;
}

export const Section = ({ color }: SectionProps) => {
  return (
    <div
      style={{
        backgroundColor: color,
        padding: "5px",
        position: "relative",
        height: "100vh",
        overflow: "hidden",
      }}
    ></div>
  );
};
