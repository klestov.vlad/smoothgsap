import { ReactNode, useRef, useLayoutEffect } from "react";

import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import { gsap } from "gsap/dist/gsap";

interface ParallaxWrapperProps {
  children: ReactNode;
  opacity?: number;
  shiftY?: number;
}

export const ParallaxWrapper = ({
  children,
  opacity = 0.3,
  shiftY = -250,
}: ParallaxWrapperProps) => {
  const rootRef = useRef<HTMLDivElement>(null);

  const refContent = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    ScrollTrigger.defaults({
      immediateRender: false,
    });
    ScrollTrigger.normalizeScroll(true);

    const context = gsap.context(() => {
      const tl1 = gsap.timeline({
        scrollTrigger: {
          trigger: rootRef.current,
          start: `bottom bottom`,
          scrub: 1,
          pin: true,
          pinSpacing: false,
          markers: true,
        },
      });

      tl1.to(refContent.current, {
        // y: shiftY,
        opacity,
      });
    }, rootRef);

    return () => context.revert();
  }, []);

  return (
    <section ref={rootRef}>
      <div ref={refContent}>{children}</div>
    </section>
  );
};
