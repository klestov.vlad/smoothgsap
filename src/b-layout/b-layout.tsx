import { ReactNode } from "react";
import { BVirtualScroll } from "../b-virtual-scroll/b-virtual-scroll";

interface BLayoutProps {
  children: ReactNode;
}

export const BLayout = ({ children }: BLayoutProps) => {
  const p = "b-layout";

  return (
    <main className={`${p}`}>
      <BVirtualScroll>{children}</BVirtualScroll>
    </main>
  );
};
