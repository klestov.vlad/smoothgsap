import "../styles/globals.css";
import type { AppProps } from "next/app";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";

import { gsap } from "gsap/dist/gsap";
import "locomotive-scroll/dist/locomotive-scroll.css";

gsap.registerPlugin(ScrollTrigger);

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}
