import LocomotiveScroll from "locomotive-scroll";
import { useEffect } from "react";
import { Section } from "../src";
import { BLayout } from "../src/b-layout/b-layout";
import { ParallaxWrapper } from "../src/components/ParallaxWrapper";

const colors = ["green", "orange", "blue", "red"];

export default function Home() {
  return (
    <BLayout>
      <div>
        {colors.map((color, index) => (
          <ParallaxWrapper key={index}>
            <Section color={color} />
          </ParallaxWrapper>
        ))}
      </div>
    </BLayout>
  );
}
